import java.applet.Applet;
import java.awt.Button;
import java.awt.Graphics;
import java.awt.Label;
import java.awt.TextField;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.sql.DriverManager;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.ArrayList;

import com.mysql.jdbc.Connection;
import com.mysql.jdbc.Statement;

public class JavaLogin extends Applet{
	private static final long serialVersionUID = 1L;
	
	private Connection con;
	
	//Connection variables
	//Laptop URL: jdbc:mysql://localhost:3306/test
	//Mac URL: jdbc:mysql://localhost:8809/test
	private String url = "jdbc:mysql://localhost:8889/test";
	private String user = "root";
	private String password = "root";
	
	//Logged-in menu
	private Button btnLogout;
	
	//Login menu
	private Button btnLogin;
	private TextField txtUsername;
	private TextField txtPassword;
	private Label labUsername;
	private Label labPassword;
	private Label labStatus;
	
	//Register menu
	private Button btnRegister;
	
	public void init(){
		initSql();
		
		txtUsername = new TextField(20);
		txtPassword = new TextField(20);
		labUsername = new Label("Username: ");
		labPassword = new Label("Password: ");
		labStatus = new Label("");
		
		btnLogin = new Button("Login");
		btnLogin.addActionListener(new ActionListener(){

			@Override
			public void actionPerformed(ActionEvent arg) {
				if(txtUsername.getText() == "" || txtPassword.getText() == ""){
					labStatus.setText("You didn't enter your username or password!");
					return;
				}
				
				if(executeQuery("SELECT * FROM users WHERE users.username=\"" + txtUsername.getText() + "\"", 2).size() == 0){
					labStatus.setText("That user does not exist!");
					return;
				}
				
				String name = executeQuery("SELECT * FROM users WHERE users.username=\"" + txtUsername.getText() + "\"", 2).get(0);
				String pass = executeQuery("SELECT * FROM users WHERE users.username=\"" + txtUsername.getText() + "\"", 3).get(0);
				
				if(txtPassword.getText().equals(pass)){
					txtUsername.setText("");
					txtPassword.setText("");
					
					remove(labUsername);
					remove(txtUsername);
					remove(labPassword);
					remove(txtPassword);
					remove(btnLogin);
					remove(btnRegister);
					
					btnLogout.setVisible(true);
					labStatus.setText("Welcome, " + name + ". You are now logged in.");
					repaint();
				} else {
					labStatus.setText("Wrong password motherflipper!");
					repaint();
				}
			}
		});
		
		btnLogout = new Button("Logout");
		btnLogout.setVisible(false);
		btnLogout.addActionListener(new ActionListener(){

			@Override
			public void actionPerformed(ActionEvent arg) {
				add(labUsername);
				add(txtUsername);
				add(labPassword);
				add(txtPassword);
				add(btnLogin);
				add(btnRegister);
				
				btnLogout.setVisible(false);
				labStatus.setText("You just logged out!");
			}
		});
		
		btnRegister = new Button("Register");
		btnRegister.addActionListener(new ActionListener(){

			@Override
			public void actionPerformed(ActionEvent arg) {
				if(txtUsername.getText() == "" || txtPassword.getText() == ""){
					labStatus.setText("You didn't enter your username or password!");
					return;
				}
				
				if(txtUsername.getText().length() > 20 || txtPassword.getText().length() > 20){
					labStatus.setText("Your name and password can only be 20 characters long!");
					return;
				}
				
				if(executeQuery("SELECT * FROM users WHERE users.username=\"" + txtUsername.getText() + "\"", 2).size() > 0){
					labStatus.setText("That username already exists!");
					return;
				}
				
				executeQuery("insert into users(username, password) values ('" + txtUsername.getText() + "', '" + txtPassword.getText() + "');");
			
				labStatus.setText("Account created!");
			}
		});

		this.add(labStatus);
		this.add(btnLogout);
		this.add(labUsername);
		this.add(txtUsername);
		this.add(labPassword);
		this.add(txtPassword);
		this.add(btnLogin);
		this.add(btnRegister);
		
		for(String str : executeQuery("SELECT * FROM users", 2)){
			System.out.println(str);
		}
	}
	
	public void paint(Graphics g){
		
	}
	
	private void executeQuery(String query){
		try {
			Statement state = (Statement) con.createStatement();
			state.executeUpdate(query);
			
			state.close();
		} catch (SQLException e) {
			e.printStackTrace();
		}
	}
	
	
	private ArrayList<String> executeQuery(String query, int col){
		ArrayList<String> resReturn = new ArrayList<String>();
		
		try {
			ResultSet result;
			Statement state = (Statement) con.createStatement();
			result = state.executeQuery(query);
			
			while(result.next()){
				resReturn.add(result.getString(col));
			}
			
			state.close();
			result.close();
		} catch (SQLException e) {
			e.printStackTrace();
			return null;
		}
		
		return resReturn;
	}
	
	private boolean initSql(){
		try {
			Class.forName("com.mysql.jdbc.Driver");
			con = (Connection) DriverManager.getConnection(url, user, password);
		} catch (Exception e) {
			e.printStackTrace();
			return false;
		}
		
		return true;
	}
}
